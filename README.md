# Kyle's character backstory starter

Keltherion was a soldier from Brashvander.  Trained in battle from a young age,
he entered the Orc Wars on Awen at the age of 12 and fought until the war's end
11 years later.

After returning home from battle in the Valley of Orcs, he met a beautiful elven
woman, and settled down.  Not long after they were married, the woman was
cornered, raped and killed by six travelers.  Outraged, Keltherion tracked them
down and killed them in the street in broad daylight.  He was arrested and
imprisoned, but released shortly after due to the laws of vengeance in
Brashvander.

He went home to try and pick up the pieces, he felt it was too difficult to
continue living in the home they built together.  With reminders of his lost
wife everywhere, he left the city.

At 25, he found himself in Ekkr, needing employment.  Not having known anything
other than battle, he fell in with a sort of crime syndicate called the Decaying
Maelstrom.  As band of theives, they felt Kel's expertise would benefit them
greatly, and it did.  Kel, was awarded high pay and high esteem within the
organization for his abilities in combat and success rate of all the jobs he was
assigned to.

All of that changed in one day after nearly 11 years, when Greylen, the leader
of the Decaying Maelstrom, decided to assassinate the leader of Ekkr to take
control of the city.  Kel was tasked with two others to steal something from the
leader's estate at night, the main plot remaining unknown to Kel.  While he was
securing the area, the other two snuck upstairs and killed the leader, his wife
and children.  The two assassins met Kel downstairs with the items they were
sent to retrieve, and three left together.  The next day, when word got out that
the leader and his family had been killed the previous night, Kel was enraged,
and confronted Greylen, but the conflict went unresolved, as their meeting was
interrupted by talk of an opportunity for some quick wealth about to leave town.

Immediately, Kel was tasked with another to go aboard a ship to steal its cargo
before it had a chance to leave the port.  Kel saw his opportunity to part from
this band of murders, so he knocked his partner unconscience and left him bound
in behind some crates on the dock, and boarded the ship to leave Ekkr.
